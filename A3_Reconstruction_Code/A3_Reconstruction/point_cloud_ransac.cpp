#include "point_cloud_ransac.h"

#include <set>
#include <list>

#include <easy3d/point_cloud.h>

#include <3rd_party/RANSAC/RansacShapeDetector.h>
#include <3rd_party/RANSAC/PlanePrimitiveShapeConstructor.h>
#include <3rd_party/RANSAC/CylinderPrimitiveShapeConstructor.h>
#include <3rd_party/RANSAC/SpherePrimitiveShapeConstructor.h>
#include <3rd_party/RANSAC/ConePrimitiveShapeConstructor.h>
#include <3rd_party/RANSAC/TorusPrimitiveShapeConstructor.h>
#include <3rd_party/RANSAC/PlanePrimitiveShape.h>
#include <3rd_party/RANSAC/SpherePrimitiveShape.h>
#include <3rd_party/RANSAC/CylinderPrimitiveShape.h>
#include <3rd_party/RANSAC/ConePrimitiveShape.h>
#include <3rd_party/RANSAC/TorusPrimitiveShape.h>

//OMG, there is class with exactly the same name in RANSAC!!!
typedef  ::PointCloud   PointCloud_Ransac;

namespace easy3d {


    // returns the number of detected primitives
    int do_detect(
        PointCloud* cloud,
        PointCloud_Ransac& pc,
        unsigned int min_support,
        float dist_thresh
        )
    {
        float bitmap_reso = 0.02f;	// relative to the bounding box width. NOTE: This threshold is NOT multiplied internally!
        float normal_thresh = 0.8f;	// the cos of the maximal normal deviation
        float overlook_prob = 0.001f;	// the probability with which a primitive is overlooked

        const Box3& box = cloud->bounding_box();
        pc.setBBox(
            Vec3f(static_cast<float>(box.x_min()), static_cast<float>(box.y_min()), static_cast<float>(box.z_min())),
            Vec3f(static_cast<float>(box.x_max()), static_cast<float>(box.y_max()), static_cast<float>(box.z_max()))
            );

        //////////////////////////////////////////////////////////////////////////

        std::cout << "detecting primitives..." << std::endl;
//        StopWatch w;

        RansacShapeDetector::Options ransacOptions;
        ransacOptions.m_minSupport = min_support;
        ransacOptions.m_epsilon = dist_thresh * pc.getScale();
        ransacOptions.m_bitmapEpsilon = bitmap_reso * pc.getScale();
        ransacOptions.m_normalThresh = normal_thresh;
        ransacOptions.m_probability = overlook_prob;

        RansacShapeDetector detector(ransacOptions); // the detector object

        // set which primitives are to be detected by adding the respective constructors
        detector.Add(new PlanePrimitiveShapeConstructor());

        MiscLib::Vector< std::pair< MiscLib::RefCountPtr< PrimitiveShape >, size_t > > shapes; // stores the detected shapes
        // returns number of unassigned points
        // the array shapes is filled with pointers to the detected shapes
        // the second element per shapes gives the number of points assigned to that primitive (the support)
        // the points belonging to the first shape (shapes[0]) have been sorted to the end of pc,
        // i.e. into the range [ pc.size() - shapes[0].second, pc.size() )
        // the points of shape i are found in the range
        // [ pc.size() - \sum_{j=0..i} shapes[j].second, pc.size() - \sum_{j=0..i-1} shapes[j].second )
        std::size_t remaining = detector.Detect(pc, 0, pc.size(), &shapes); // run detection

        PointCloud_Ransac::reverse_iterator start = pc.rbegin();
        PointCloud_Ransac::reverse_iterator end = pc.rend();
        MiscLib::Vector< std::pair< MiscLib::RefCountPtr< PrimitiveShape >, std::size_t > >::const_iterator shape_itr = shapes.begin();

        auto lables = cloud->vertex_property<int>("v:segment_label", -1);
        auto indices = cloud->vertex_property<int>("v:segment_index", -1);
        int segment_index = 0;
        for (unsigned int id = 0; shape_itr != shapes.end(); ++shape_itr, ++id) {
            const PrimitiveShape* primitive = shape_itr->first;
            std::size_t num = shape_itr->second;

            std::list<int> vts;
            PointCloud_Ransac::reverse_iterator point_itr = start;
            for (std::size_t count = 0; count < num; ++count) {
                int v = int(point_itr->index);
                vts.push_back(v);
                ++point_itr;
            }
            start = point_itr;

            // Liangliang: I have to check Ruwen Schnabel's source code of RANSAC. Can the
            //			   returned primitive have a support point number less than min_support?
            //			   If so, just skip it.
            if (num < min_support)
                continue;

            // extract parameters for this primitive
            for (auto id : vts) {
                const PointCloud::Vertex v(id);
                indices[v] = segment_index;
            }
            ++segment_index;
        }


        std::cout << segment_index << " primitives extracted. " << remaining << " points remained" << std::endl;
        return segment_index;
    }




    int PrimitivesRansac::detect(
        PointCloud* cloud,
        unsigned int min_support /* = 1000 */,
        float dist_thresh /* = 0.005 */)
    {
        if (!cloud) {
            std::cerr << "no data exists" << std::endl;
            return 0;
        }

        if (cloud->n_vertices() < 3) {
            std::cerr << "point set has less than 3 points" << std::endl;
            return 0;
        }

        PointCloud::VertexProperty<vec3> normals = cloud->get_vertex_property<vec3>("v:normal");
        if (!normals) {
            std::cerr << "RANSAC Detector requires point cloud normals" << std::endl;
            return 0;
        }

        // prepare the data
        PointCloud_Ransac pc;
        pc.resize(cloud->n_vertices());

        const std::vector<vec3>& nms = normals.vector();
        const std::vector<vec3>& pts = cloud->points();
    #pragma omp parallel for
        for (int i = 0; i < pts.size(); ++i) {
            const vec3& p = pts[i];
            const vec3& n = nms[i];
            pc[i] = Point(
                Vec3f(p.x, p.y, p.z),
                Vec3f(n.x, n.y, n.z)
                );
            pc[i].index = i;
        }

        return do_detect(cloud, pc, min_support, dist_thresh);
    }

}
