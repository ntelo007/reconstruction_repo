/*
*	Copyright (C) 2015 by Liangliang Nan (liangliang.nan@gmail.com)
*	https://3d.bk.tudelft.nl/liangliang/
*
*	This file is part of the GEO1004 assignment code framework.
*/

#include "my_viewer.h"

#include <3rd_party/glew/include/GL/glew.h>		// for OpenGL functions
#include <3rd_party/glfw/include/GLFW/glfw3.h>	// for glfw functions


#include <easy3d/drawable.h>
#include <easy3d/point_cloud.h>

#include "normal_estimator.h"
#include "point_cloud_ransac.h"
#include <iostream>


MyViewer::MyViewer()
    : easy3d::Viewer("A3_Reconstruction")
{
    std::cout <<
                 "-------------------------------------------------------------------\n"
                 "  E:               Estimation point cloud normals                  \n"
                 "  N:               Toggle the visualization of the normals" << std::endl;

    std::cout <<
                 "-------------------------------------------------------------------\n"
                 "  B:               Extract planes                                 \n"<< std::endl;

}

MyViewer::~MyViewer()
{
}


bool MyViewer::key_press_event(int key, int modifiers)
{
    if (key == GLFW_KEY_E) {
        easy3d::PointCloud* cloud = point_cloud();
        NormalEstimator estimator;
        estimator.apply(cloud);

        easy3d::PointCloud::VertexProperty<easy3d::vec3> normals = cloud->get_vertex_property<easy3d::vec3>("v:normal");
        if (normals) { // normal estimation may fail, make sure the normals exist
            // -------------------------------------------------------
            // now we have the normal information, we can have a better
            // visualization of the point cloud. But we first need to
            // upload the normal information to GPU.
            easy3d::PointsDrawable* drawable = cloud->points_drawable("points");
            drawable->update_normal_buffer(normals.vector());

            // -------------------------------------------------------
            // this is for visualizing the normal vectors
            creat_normal_drawable(cloud);
        }

        return true;
    }

    else if (key == GLFW_KEY_N) {
        easy3d::PointCloud* cloud = point_cloud();
        if (cloud) {
            easy3d::LinesDrawable* lines = cloud->lines_drawable("normals");
            if (lines) {
                bool visible = lines->is_visible();
                lines->set_visible(!visible);
                update();
            }
            else
                creat_normal_drawable(cloud);
        }
        else
            std::cerr << "Warning: point cloud does not exist" << std::endl;
        return true;
    }

    //The call of the function to extract planes from a point cloud with normals
    else if (key == GLFW_KEY_B) {
        //this if statements uses the point cloud that is imported inside the viewer
        easy3d::PointCloud* cloud = point_cloud();

        //calling of the extract_planes function that is described in the header file
        extract_planes(cloud);
        return true;

    }
    else
        return Viewer::key_press_event(key, modifiers);
}


// Returns the pointer of the loaded point cloud.
// In case the model doesn't exist (e.g., not loaded to the viewer),
// it will return NULL (== 0).
easy3d::PointCloud* MyViewer::point_cloud()
{
    easy3d::PointCloud* cloud = dynamic_cast<easy3d::PointCloud*>(current_model());
    return cloud;
}


// create a drawable for visualizing the normals.
void MyViewer::creat_normal_drawable(easy3d::PointCloud* cloud)
{
    // it is always good to check if the input is valid
    if (cloud == nullptr) {
        std::cerr << "Warning: point cloud does not exist" << std::endl;
        return;
    }

    // the point normals (in type 'vec3') are stored in the "v:normal" property.
    easy3d::PointCloud::VertexProperty<easy3d::vec3> normals = cloud->get_vertex_property<easy3d::vec3>("v:normal");
    if (!normals) {
        std::cerr << "Warning: normal information does not exist" << std::endl;
        return;
    }

    // create a drawable and upload the necessary data to GPU to visualize the normals.
    // Note: a user may press the same button (i.e., running this function multiple times,
    //       so a good software implementation should check if the drawable already exists.
    //          - if no, create the drawable
    //          - if yes, no need to create the drawable, but just update the data transfered to
    //            the GPU to update the visualization.
    // ach drawable should have a unique name. Here we use "normals".
    easy3d::LinesDrawable* lines = cloud->lines_drawable("normals");
        if (!lines) {// this is equal to "lines == nullptr"
            std::cout << "creating drawable...[not implemented yet]" << std::endl;
            lines = cloud->add_lines_drawable("normals");
     }

    //-----------------------------------------------------------------
    // TODO: prepare the normal data (each normal is a line segment
    //       represented by its two end points). You can store them in
    //       a array of "std::vector". Check here for its usage:
    //       https://en.cppreference.com/w/cpp/container/vector
	easy3d::PointCloud::VertexProperty<easy3d::vec3> points = cloud->get_vertex_property<easy3d::vec3>("v:point");
	/*easy3d::Box3 box;
	for (auto v : cloud->vertices())
		box.add_point(points[v]);
	float length = norm(box.max() - box.min()) * 0.01f;*/
    float length = 0.5f;

	// Every consecutive two points represent a normal vector.
	std::vector<easy3d::vec3> normal_points;
	for (auto v : cloud->vertices()) {
		const easy3d::vec3& s = points[v];
		easy3d::vec3 n = normals[v];
		n.normalize();
		const easy3d::vec3& t = points[v] + n * length;
		normal_points.push_back(s);
		normal_points.push_back(t);
	}
    //-----------------------------------------------------------------
    // TODO: upload the normal data into GPU by call the drawable's
    //       update_vertex_buffer() function.
	easy3d::LinesDrawable* normals_drawable = cloud->add_lines_drawable("normals");

    // see Tutorial_11_VectorFields for an example
	normals_drawable->update_vertex_buffer(normal_points);
	// We will draw the normal vectors in green color
	normals_drawable->set_per_vertex_color(false);
	normals_drawable->set_default_color(easy3d::vec3(1.0f, 0.0f, 0.0f));
    // update the viewer. Then you should be able to see the normals.
    update();
}

easy3d::vec3 random_color() {
    float r = rand() % 255 / 255.0f;	// in the range [0, 1]
    float g = rand() % 255 / 255.0f;	// in the range [0, 1]
    float b = rand() % 255 / 255.0f;	// in the range [0, 1]
    return easy3d::vec3(r, g, b);
}
//extract planes from oriented point cloud
void MyViewer::extract_planes(easy3d::PointCloud *cloud) {
    double i;
    unsigned int min_support = 1000;
    float dist_thresh = 0.005f;
    std::cout << "Please enter an integer value for the minimum number of points (default = 1000): ";
    std::cin >> i;
    if (i>50) {
        min_support = i;
    }
    std::cout << "Please enter a distance threshold value (default = 0.005): ";
    std::cin >> i;
    if (i<0.01f) {
        dist_thresh = i;
    }
    easy3d::PrimitivesRansac ransac;
    std::cout << "input parameters: " << min_support << " and " << dist_thresh << std::endl;
    int num_primitives = ransac.detect(cloud,min_support,dist_thresh);
    std::cout << "The number of priminitives is " << num_primitives << std::endl;

    if (num_primitives>0) {
    // Assign colors to primitives

    //get the identifier of the segment property of every vertex
    auto indices = cloud->vertex_property<int>("v:segment_index", -1);


    //assign a color to every vertex of the point cloud
    std::cout << "Assigning color to vertices..." <<std::endl;

    //check if the point cloud has the color property, otherwise create it
    easy3d::PointCloud::VertexProperty<easy3d::vec3> colors = cloud->get_vertex_property<easy3d::vec3>("v:color");
    if (!colors) {
        easy3d::PointCloud::VertexProperty<easy3d::vec3> colors = cloud->add_vertex_property<easy3d::vec3>("v:color");
        for (int n=0; n<num_primitives; n++) {
            easy3d::vec3 color = random_color();
            for (auto v : cloud->vertices()) {
                if (indices[v]==n) {
                    colors[v] = color;
                }
            }
    }
    }
    else {
        for (int n=0; n<num_primitives; n++) {
            easy3d::vec3 color = random_color();
            for (auto v : cloud->vertices()) {
                if (indices[v]==n) {
                    colors[v] = color;
                }
            }
    }

    }
    std::cout << "Coloring finished!" << std::endl;

    auto drawable = cloud->points_drawable("points");
    drawable->update_color_buffer(colors.vector());
    std::cout << "Colors are updated in the viewer" << std::endl;
    //Updates the viewer
    update();
}
    else {
        std::cout << "Primitives could not be found. Try to calculate normals." << std::endl;
    }
}





