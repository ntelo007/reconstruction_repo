/*
*	Copyright (C) 2015 by Liangliang Nan (liangliang.nan@gmail.com)
*	https://3d.bk.tudelft.nl/liangliang/
*
*	This file is part of Easy3D: software for processing and rendering
*   meshes and point clouds.
*
*	Easy3D is free software; you can redistribute it and/or modify
*	it under the terms of the GNU General Public License Version 3
*	as published by the Free Software Foundation.
*
*	Easy3D is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program. If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef EASY3D_ALGO_RANSAC_H
#define EASY3D_ALGO_RANSAC_H


#include <set>
#include <vector>


namespace easy3d {

    class PointCloud;

    // usage:
    //    PrimitivesRansac ransac;
    //    int num = ransac.detect(cloud);

    class PrimitivesRansac
    {
    public:
        // extract primitive from the point cloud.
        // returns the nummber of extracted primitives.
        // result will be stored as properties:
        //      - "v:segment_index" (0, 1, 2...)
        int detect(
            PointCloud* cloud,
            unsigned int min_support = 1000,	// the minimal number of points required for a primitive
            float dist_thresh = 0.005f          // relative to the bounding box size
            );
    };

}


#endif  //  EASY3D_ALGO_RANSAC_H
