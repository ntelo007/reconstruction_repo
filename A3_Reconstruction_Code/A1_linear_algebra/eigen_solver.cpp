#include "eigen_solver.h"



EigenSolver::EigenSolver()
{

}


EigenSolver::~EigenSolver()
{

}


// compute the eigen values and eigen vectors of matrix m.
void EigenSolver::solve(const mat3& m) {
    // after computation:
    //  - the eigen values are stored in the member "m_eigen_values"
    //  - the eigen vectors are stored in the member "m_eigen_vectors"

    // TODO -> call the function defined in "eigen_symmetric.h"
    // Please read carefully the manual of the function.
}
